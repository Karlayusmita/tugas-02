import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class matriks {

	public static void menu(){
		System.out.println("Operasi pada Matriks");
		System.out.println("1. Penjumlahan Dua Matriks");
		System.out.println("2. Pengurangan Dua Matriks");
		System.out.println("3. Perkalian Dua Matriks");
		System.out.println("4. Mencari Determinan Matriks");
		System.out.println("0. Keluar");
	}
	
	public static void penjumlahan(){
		System.out.println("Syarat agar dua matriks dapat dijumlahkan adalah ordo matriks harus sama");
		try{
			System.out.println("Silahkan masukkan nilai pada matriks pertama");
			System.out.print("Silahkan masukkan jumlah baris pada matriks = ");
			BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputBarisA = null;
			try{
				inputBarisA = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			System.out.print("Silahkan masukkan jumlah kolom pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputkolomA = null;
			try{
				inputkolomA = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			int barisA = Integer.parseInt(inputBarisA);
			int kolomA = Integer.parseInt(inputkolomA);
			int matriksA[][]= new int[barisA][kolomA];
			for(int indeksbaris=0; indeksbaris<barisA;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
					bufferedreader = new BufferedReader (new InputStreamReader(System.in));
					String inputmatriksA = null;
					try{
						inputmatriksA= bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Error Input "+ error.getMessage());
					}
					
					try{
						int matriks = Integer.parseInt(inputmatriksA);
						matriksA[indeksbaris][indekskolom]= matriks;
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah menginput data");
					}	
				}
			}
			
			System.out.println("Matriks Pertama adalah ");
			for(int indeksbaris=0;indeksbaris<barisA;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
					System.out.print(matriksA[indeksbaris][indekskolom]+ " ");
				}
				System.out.println();
			}
			
			System.out.println("Silahkan masukkan nilai pada matriks kedua");
			System.out.print("Silahkan masukkan jumlah baris pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputBarisB = null;
			try{
				inputBarisB = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			System.out.print("Silahkan masukkan jumlah kolom pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputkolomB = null;
			try{
				inputkolomB = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			int barisB = Integer.parseInt(inputBarisB);
			int kolomB = Integer.parseInt(inputkolomB);
			int matriksB[][]= new int[barisB][kolomB];
			for(int indeksbaris=0; indeksbaris<barisB;indeksbaris++){					
				for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
					bufferedreader = new BufferedReader (new InputStreamReader(System.in));
					String inputmatriksB = null;
					try{
						inputmatriksB= bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Error Input "+ error.getMessage());
					}
						
					try{
						int matriks = Integer.parseInt(inputmatriksB);
						matriksB[indeksbaris][indekskolom]= matriks;
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah menginput data");
					}	
				}
			}
				
			System.out.println("Matriks kedua adalah ");
			for(int indeksbaris=0;indeksbaris<barisB;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
					System.out.print(matriksB[indeksbaris][indekskolom]+ " ");
				}
				System.out.println();
			}
			
			if(barisA==barisB && kolomA==kolomB){
				System.out.println("\nHasil pengurangannya adalah ");
				int matriksC[][] = new int[barisA][kolomA];
				for(int indeksbaris=0;indeksbaris<barisA;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
							matriksC[indeksbaris][indekskolom]+=matriksA[indeksbaris][indekskolom] - matriksB[indeksbaris][indekskolom];
						System.out.print(matriksC[indeksbaris][indekskolom]+"\t");
				}
				System.out.println();
				}
			}
			else
			System.out.println("Tidak dapat melakukan perkalian karena tidak memenuhi syarat");
	}
		catch(NumberFormatException e){
			System.out.println("Anda Salah menginput data");
		}
	}
	
	public static void pengurangan(){
		System.out.println("Syarat agar dua matriks dapat dijumlahkan adalah ordo matriks harus sama");
		try{
			System.out.println("Silahkan masukkan nilai pada matriks pertama");
			System.out.print("Silahkan masukkan jumlah baris pada matriks = ");
			BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputBarisA = null;
			try{
				inputBarisA = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			System.out.print("Silahkan masukkan jumlah kolom pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputkolomA = null;
			try{
				inputkolomA = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			int barisA = Integer.parseInt(inputBarisA);
			int kolomA = Integer.parseInt(inputkolomA);
			int matriksA[][]= new int[barisA][kolomA];
			for(int indeksbaris=0; indeksbaris<barisA;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
					bufferedreader = new BufferedReader (new InputStreamReader(System.in));
					String inputmatriksA = null;
					try{
						inputmatriksA= bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Error Input "+ error.getMessage());
					}
					
					try{
						int matriks = Integer.parseInt(inputmatriksA);
						matriksA[indeksbaris][indekskolom]= matriks;
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah menginput data");
					}	
				}
			}
			
			System.out.println("Matriks Pertama adalah ");
			for(int indeksbaris=0;indeksbaris<barisA;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
					System.out.print(matriksA[indeksbaris][indekskolom]+ " ");
				}
				System.out.println();
			}
			
			System.out.println("Silahkan masukkan nilai pada matriks kedua");
			System.out.print("Silahkan masukkan jumlah baris pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputBarisB = null;
			try{
				inputBarisB = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			System.out.print("Silahkan masukkan jumlah kolom pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputkolomB = null;
			try{
				inputkolomB = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			int barisB = Integer.parseInt(inputBarisB);
			int kolomB = Integer.parseInt(inputkolomB);
			int matriksB[][]= new int[barisB][kolomB];
			for(int indeksbaris=0; indeksbaris<barisB;indeksbaris++){					
				for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
					bufferedreader = new BufferedReader (new InputStreamReader(System.in));
					String inputmatriksB = null;
					try{
						inputmatriksB= bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Error Input "+ error.getMessage());
					}
						
					try{
						int matriks = Integer.parseInt(inputmatriksB);
						matriksB[indeksbaris][indekskolom]= matriks;
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah menginput data");
					}	
				}
			}
				
			System.out.println("Matriks kedua adalah ");
			for(int indeksbaris=0;indeksbaris<barisB;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
					System.out.print(matriksB[indeksbaris][indekskolom]+ " ");
				}
				System.out.println();
			}
			
			if(barisA==barisB && kolomA==kolomB){
				System.out.println("\nHasil penjumlahannya adalah ");
				int matriksC[][] = new int[barisA][kolomA];
				for(int indeksbaris=0;indeksbaris<barisA;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
							matriksC[indeksbaris][indekskolom]+=matriksA[indeksbaris][indekskolom]+matriksB[indeksbaris][indekskolom];
						System.out.print(matriksC[indeksbaris][indekskolom]+"\t");
				}
				System.out.println();
				}
			}
			else
			System.out.println("Tidak dapat melakukan perkalian karena tidak memenuhi syarat");
	}
		catch(NumberFormatException e){
			System.out.println("Anda Salah menginput data");
		}
	}
	
	public static void perkalian(){
		System.out.println("Syarat agar dua matriks dapat dikalikan adalah baris\npada matriks pertama harus sama dengan kolom pada matriks ke dua\n");
		try{
			System.out.println("Silahkan masukkan nilai pada matriks pertama");
			System.out.print("Silahkan masukkan jumlah baris pada matriks = ");
			BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputBarisA = null;
			try{
				inputBarisA = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			System.out.print("Silahkan masukkan jumlah kolom pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputkolomA = null;
			try{
				inputkolomA = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			int barisA = Integer.parseInt(inputBarisA);
			int kolomA = Integer.parseInt(inputkolomA);
			int matriksA[][]= new int[barisA][kolomA];
			for(int indeksbaris =0; indeksbaris<barisA; indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
					bufferedreader = new BufferedReader (new InputStreamReader(System.in));
					String inputmatriksA = null;
					try{
						inputmatriksA= bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Error Input "+ error.getMessage());
					}
					
					try{
						int matriks = Integer.parseInt(inputmatriksA);
						matriksA[indeksbaris][indekskolom]= matriks;
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah menginput data");
					}	
				}
			}
			
			System.out.println("Matriks Pertama adalah ");
			for(int indeksbaris=0;indeksbaris<barisA;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
					System.out.print(matriksA[indeksbaris][indekskolom]+ " ");
				}
				System.out.println();
			}
			
			System.out.println("Silahkan masukkan nilai pada matriks kedua");
			System.out.print("Silahkan masukkan jumlah baris pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputBarisB = null;
			try{
				inputBarisB = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			System.out.print("Silahkan masukkan jumlah kolom pada matriks = ");
			bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputkolomB = null;
			try{
				inputkolomB = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			int barisB = Integer.parseInt(inputBarisB);
			int kolomB = Integer.parseInt(inputkolomB);
			int matriksB[][]= new int[barisB][kolomB];
			for(int indeksbaris=0; indeksbaris<barisB;indeksbaris++){					
				for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
					System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
					bufferedreader = new BufferedReader (new InputStreamReader(System.in));
					String inputmatriksB = null;
					try{
						inputmatriksB= bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Error Input "+ error.getMessage());
					}
						
					try{
						int matriks = Integer.parseInt(inputmatriksB);
						matriksB[indeksbaris][indekskolom]= matriks;
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah menginput data");
					}	
				}
			}
				
			System.out.println("Matriks kedua adalah ");
			for(int indeksbaris=0;indeksbaris<barisB;indeksbaris++){
				for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
					System.out.print(matriksB[indeksbaris][indekskolom]+ " ");
				}
				System.out.println();
			}
			
			if(kolomA==barisB){
				System.out.println("Karena baris pada matriks pertama harus \nsama dengan kolom pada matriks ke dua sama, maka ");
				System.out.println("\nHasil perkaliannya adalah ");
				int matriksC[][] = new int[barisA][kolomB];
				for(int i=0;i<barisA;i++){
					for(int indekskolom=0; indekskolom<kolomB;indekskolom++){
						for (int u=0; u<kolomA; u++){
							matriksC[i][indekskolom]+=matriksA[i][u]*matriksB[u][indekskolom];
						}
						System.out.print(matriksC[i][indekskolom]+"   ");
				}
				System.out.println();
				}
			}
			else
			System.out.println("Tidak dapat melakukan perkalian karena tidak memenuhi syarat");
	}
		catch(NumberFormatException e){
			System.out.println("Anda Salah menginput data");
		}
	}
	
	public static void determinan(){
		System.out.println("Program ini hanya dapat menghitung determinan matriks berordo 2x2 dan 3x3");
		System.out.println("Silahkan pilih ordo matriks yang ingin dihitung determinannya");
		System.out.println("1. 2x2\t2. 3x3");

		try{
			System.out.print("Masukkan pilihan Anda = ");
			BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
			String inputpilihan= null;
			try{
				inputpilihan = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Error Input "+ error.getMessage());
			}
			
			try{
				int pilih = Integer.parseInt(inputpilihan);
				if(pilih == 1){
					int barisA = 2;
					int kolomA = 2;
					int matriksA[][]= new int[barisA][kolomA];
					for(int indeksbaris=0; indeksbaris<barisA;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
							System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
							bufferedreader = new BufferedReader (new InputStreamReader(System.in));
							String inputmatriksA = null;
							try{
								inputmatriksA= bufferedreader.readLine();
							}
							catch(IOException error){
								System.out.println("Error Input "+ error.getMessage());
							}
							
							try{
								int matriks = Integer.parseInt(inputmatriksA);
								matriksA[indeksbaris][indekskolom]= matriks;
							}
							catch(NumberFormatException e){
								System.out.println("Anda salah menginput data");
							}	
						}
					}
					
					System.out.println("Matriks adalah ");
					for(int indeksbaris=0;indeksbaris<barisA;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
							System.out.print(matriksA[indeksbaris][indekskolom]+ " ");
						}
						System.out.println();
					}
					
					double hasil;
					hasil = (matriksA[0][0]*matriksA[1][1])-(matriksA[0][1]*matriksA[1][0]);
					System.out.println("Determinan = (" + matriksA[0][0] + "*"+matriksA[1][1]+ ")-("+matriksA[0][1]+"*"+matriksA[1][0]+")");
					System.out.println("Determinan dari matriks diatas adalah "+ hasil);
				}
				else if(pilih == 2){
					int barisA = 3;
					int kolomA = 3;
					int matriksA[][]= new int[barisA][kolomA];
					for(int indeksbaris=0; indeksbaris<barisA;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
							System.out.print("Baris "+(indeksbaris+1)+" kolom "+(indekskolom+1)+" = ");
							bufferedreader = new BufferedReader (new InputStreamReader(System.in));
							String inputmatriksA = null;
							try{
								inputmatriksA= bufferedreader.readLine();
							}
							catch(IOException error){
								System.out.println("Error Input "+ error.getMessage());
							}
							
							try{
								int matriks = Integer.parseInt(inputmatriksA);
								matriksA[indeksbaris][indekskolom]= matriks;
							}
							catch(NumberFormatException e){
								System.out.println("Anda salah menginput data");
							}	
						}
					}
					
					System.out.println("Matriks adalah ");
					for(int indeksbaris=0;indeksbaris<barisA;indeksbaris++){
						for(int indekskolom=0; indekskolom<kolomA;indekskolom++){
							System.out.print(matriksA[indeksbaris][indekskolom]+ " ");
						}
						System.out.println();
					}
					double hasil;
					hasil = (matriksA[0][0]*matriksA[1][1]*matriksA[2][2]) + (matriksA[0][1]*matriksA[1][2]*matriksA[2][0]) + (matriksA[0][2]*matriksA[1][0]*matriksA[2][1]) - (matriksA[0][2]*matriksA[1][1]*matriksA[2][0]) - (matriksA[0][0]*matriksA[1][2]*matriksA[2][1]) - (matriksA[0][1]*matriksA[1][0]*matriksA[2][2]);
					System.out.println("Determinan = (" + (matriksA[0][0]) + "*" + (matriksA[1][1]) + "*" + (matriksA[2][2]) + ") + (" + (matriksA[0][1]) + "*" + (matriksA[1][2]) + "*" + (matriksA[2][0]) + ") + (" + (matriksA[0][2]) + "*" +  (matriksA[1][0]) + "*" + (matriksA[2][1]) + ") - (" + (matriksA[0][2]) + "*" + (matriksA[1][1]) + "*" + (matriksA[2][0]) +") - (" + (matriksA[0][0])+"*"+(matriksA[1][2])+"*"+(matriksA[2][1])+") - ("+(matriksA[0][1])+"*"+(matriksA[1][0])+"*"+(matriksA[2][2])+")");
					System.out.println("Determinan dari matriks diatas adalah "+ hasil);
				}
				else {
					System.out.println("Anda Salah Memasukkan Pilihan");
				}
			
			}
		catch(NumberFormatException e ){			
			System.out.println("Anda Salah memasukkan Pilihan");
		}
	}		
	catch(NumberFormatException e ){			
		System.out.println("Anda Salah memasukkan Pilihan");
	}
	}
	
	public static void main(String[] args) {
		menu();
		System.out.print("Masukkan Pilihan Anda = ");
		BufferedReader bufferedreader = new BufferedReader (new InputStreamReader (System.in));
		String inputpilih = null;
		int pilih = 0;
		try{
			inputpilih = bufferedreader.readLine();
			try{
				pilih = Integer.parseInt(inputpilih);
				if(pilih >0 && pilih == 1){
					penjumlahan();
				}
				else if (pilih > 0 && pilih == 2){
					pengurangan();
				}
				else if(pilih > 0 && pilih == 3){
					perkalian();
				}
				else if(pilih > 0 && pilih == 4){
					determinan();
				}
				else if(pilih ==0){
				}
				else{
					System.out.println("Anda salah memasukkan pilihan");
				}
			}
			catch(NumberFormatException e){
				System.out.println("Anda Salah memasukkan format data");
			}
		}
		catch(IOException error){
			System.out.println("Input Error "+ error.getMessage());
		}
	}
}
		

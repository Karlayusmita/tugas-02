import java.util.*;
import java.io.*;
public class TicTacToe {

		static Scanner scan = new Scanner(System.in);
		static void play(){
			int nomor=-1, pemain=1,pilih;
			char[][]tictactoo=new char[3][3];
			for(int baris=0;baris<3;baris++){
				for(int kolom=0;kolom<3;kolom++){
					tictactoo [baris][kolom]=' ';
				}
			}
			
			do{
				
				int pilihan2 = -1;
			do{
				String pilihan="a";
				for(int baris=0;baris<3;baris++){
					for (int kolom=0;kolom<3;kolom++){
						tictactoo [baris][kolom]=' ';
					}
				}
				System.out.print("Pilih tanda (x/o): ");
				try{
					pilihan=scan.nextLine();
					if(pilihan.contentEquals("x")==false && pilihan.contentEquals("o")==false){
						throw new InputMismatchException();
					}
				}catch(InputMismatchException e){
					System.out.println("Masukkan huruf 'X' atau 'O'");
				}
				if (pilihan.contentEquals("x")){ 
					nomor=1;
					break;
				}
				if (pilihan.contentEquals("o")){
					nomor=0;
					break;
				}
				
				}while(true);
				
				while(true){
					int isi=0;
					
					System.out.println("------------------------------------");
					System.out.println("\t Giliran pemain ke-"+pemain);
					System.out.println();
					System.out.println("\t  "+tictactoo[0][0]+"|   "+tictactoo[0][1]+"   |"+tictactoo[0][2]);
					System.out.println("\t___|_______|____");
					System.out.println("\t  "+tictactoo[1][0]+"|   "+tictactoo[1][1]+"   |"+tictactoo[1][2]);
					System.out.println("\t___|_______|____");
					System.out.println("\t  "+tictactoo[2][0]+"|   "+tictactoo[2][1]+"   |"+tictactoo[2][2]);
					System.out.println("\n");
					
					if(tictactoo[0][0]!=' '&& tictactoo[0][0]==tictactoo[0][1]&& tictactoo[0][0]==tictactoo[0][2]|| 
					tictactoo[1][0]!=' '&& tictactoo[1][0]==tictactoo[1][1]&&tictactoo[1][0]==tictactoo[1][2]|| 
					tictactoo[2][0]!=' '&& tictactoo[2][0]==tictactoo[2][1]&&tictactoo[2][0]==tictactoo[2][2]||
					tictactoo[0][0]!=' '&& tictactoo[0][0]==tictactoo[1][1]&&tictactoo[0][0]==tictactoo[2][2]||
					tictactoo[0][2]!=' '&& tictactoo[0][2]==tictactoo[1][1]&&tictactoo[0][2]==tictactoo[2][0]||
					tictactoo[0][0]!=' '&& tictactoo[0][0]==tictactoo[1][0]&&tictactoo[0][0]==tictactoo[2][0]||
					tictactoo[0][1]!=' '&& tictactoo[0][1]==tictactoo[1][1]&&tictactoo[0][1]==tictactoo[2][1]||
					tictactoo[0][2]!=' '&& tictactoo[0][2]==tictactoo[1][2]&&tictactoo[0][2]==tictactoo[2][2])
					{
						if (pemain==1)pemain++;
						else if(pemain==2)pemain--;
						System.out.println("Permainan berakhir, pemain ke-"+pemain+"Menang");
						do{
							try{
								System.out.print("Tekan 1 untuk bermain lagi dan 0 untuk keluar : ");
								pilihan2=scan.nextInt();
								
								if((pilihan2==1)== false && (pilihan2==0)== false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("~~~~~~~~~~~~~~~~~~~~~");
								System.out.println("Masukkan Angka 1 atau 0");
								scan.nextLine();
								continue;
							}break;
							}while(true);
							break;
						}
						for(int baris=0;baris<3;baris++){
							for(int kolom=0;kolom<3;kolom++){
							if(tictactoo[baris][kolom]!=' '){
								isi++;
						}
									
					}
						}if(isi==9){
							System.out.println("Permainan berakhir, kedua pemain seri");
							do{
								try{
									System.out.print("Tekan 1 untuk bermain lagi dan 0 untuk keluar");
									pilihan2=scan.nextInt();
									
									if((pilihan2==1)==false&&(pilihan2==0)==false){
										throw new InputMismatchException ();
									}
								}
								catch(InputMismatchException e){
									System.out.println("Masukkan angka 1 atau 0");
									scan.nextLine();
									continue;
								}break;
							}while(true);
							break;
							}
						
						System.out.println("Masukkan angka yang tersedia untuk mengisi :");
						if(tictactoo[0][0]==' '){
							System.out.println("1. Baris 1 kolom 1");
						}
						if(tictactoo[0][1]==' '){
							System.out.println("2. Baris 1 kolom 2");
						}
						if(tictactoo[0][2]==' '){
							System.out.println("3. Baris 1 kolom 3");
						}
						if(tictactoo[1][0]==' '){
							System.out.println("4. Baris 2 kolom 1");
						}
						if(tictactoo[1][1]==' '){
							System.out.println("5. Baris 2 kolom 2");
						}
						if(tictactoo[1][2]==' '){
							System.out.println("6. Baris 2 kolom 3");
						}
						if(tictactoo[2][0]==' '){
							System.out.println("7. Baris 3 kolom 1");
						}
						if(tictactoo[2][1]==' '){
							System.out.println("8. Baris 3 kolom 2");
						}
						if(tictactoo[2][2]==' '){
							System.out.println("9. Baris 3 kolom 3");
						}
						do{
							
							System.out.print(">>");
							pilih=-1;
							try{
								pilih=scan.nextInt();
								break;
							}catch(InputMismatchException e){
								System.out.println("Masukkan pilihan yang tersedia");
								scan.nextLine();
								pilih=-1;
								continue;
							}
						}while(true);
						
						switch(pilih){
						case 1 :
							if(tictactoo[0][0]==' '&&pilih==1){
								if(nomor==1){
									tictactoo[0][0]='x';
									nomor--;
								}
								else if (nomor==0){
									tictactoo[0][0]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if(tictactoo[0][0]!=' '&&pilih==1){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
						case 2 :
							if(tictactoo[0][1]==' '&&pilih==2){
								if(nomor==1){
									tictactoo[0][1]='x';
									nomor--;
								}
								else if(nomor==0){
									tictactoo[0][1]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if(tictactoo[0][1]!=' '&&pilih==2){
								System.out.println("Masukkan pilihan diruang yang kosong");	
							}break;
						case 3 :
							if(tictactoo[0][2]==' '&&pilih==3){
								if(nomor==1){
									tictactoo[0][2]='x';
									nomor--;
								}
								else if(nomor==0){
									tictactoo[0][2]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if(tictactoo[0][1]!=' '&&pilih==3){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
						case 4 :
							if(tictactoo[1][0]==' '&&pilih==4){
								if(nomor==1){
									tictactoo[1][0]='x';
									nomor--;	
								}
								else if(nomor==0){
									tictactoo[1][0]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if (tictactoo[1][0]!=' '&&pilih==4){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
						case 5 :
							if(tictactoo[1][1]==' '&&pilih==5){
								if(nomor==1){
									tictactoo[1][1]='x';
									nomor--;
								}
								else if(nomor==0){
									tictactoo[1][1]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if (tictactoo[1][0]!=' '&&pilih==5){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
						case 6 :
							if(tictactoo[1][2]==' '&&pilih==6){
								if(nomor==1){
									tictactoo[1][2]='x';
									nomor--;
								}
								else if(nomor==0){
									tictactoo[1][2]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if (tictactoo[1][2]!=' '&&pilih==6){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
						case 7 :
							if(tictactoo[2][0]==' '&&pilih==7){
								if(nomor==1){
									tictactoo[2][0]='x';
									nomor--;
								}
								else if(nomor==0){
									tictactoo[2][0]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if (tictactoo[2][0]!=' '&&pilih==7){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
						case 8 :
							if(tictactoo[2][1]==' '&&pilih==8){
								if(nomor==1){
									tictactoo[2][1]='x';
									nomor--;
								}
								else if(nomor==0){
									tictactoo[2][1]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if (tictactoo[2][1]!=' '&&pilih==8){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
						case 9 :
							if(tictactoo[2][2]==' '&&pilih==9){
								if(nomor==1){
									tictactoo[2][2]='x';
									nomor--;
								}
								else if(nomor==0){
									tictactoo[2][2]='o';
									nomor++;
								}
								if(pemain==1)pemain++;
								else if(pemain==2)pemain--;
							}
							else if (tictactoo[2][2]!=' '&&pilih==9){
								System.out.println("Masukkan pilihan diruang yang kosong");
							}break;
							default:
								System.out.println("Masukkan pilihan yang sesuai");
								break;
						}
					}
				if(pilihan2==1){
					continue;
				}
				else if(pilihan2==0){
					break;
				}
				
				}while(true);
			}
				public static void main (String[]args){
					do{
						int pilih=-1;
						
					System.out.println();
					System.out.println("TicTacToo");
					System.out.println("1. Main");
					System.out.println("2. Keluar");
					System.out.print("Masukkan pilihan : ");
					BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
					String inputpilih = null;
						try{
							inputpilih = bufferedreader.readLine();
							try{
								pilih = Integer.parseInt(inputpilih);
								if(pilih == 1){
							System.out.println("Masukkan angka 1 atau 0");
							}
							else if (pilih == 2){
								break;
							}
							else{
								System.out.println("Anda Salah Memasukkan pilihan");
							}
							}
							catch(NumberFormatException e){
								System.out.println("Anda Salah memasukkan format data");
							}
						
					}catch(IOException error){
						System.out.println("Input Error"+error.getMessage());
						
					}
					play();
					
					if (pilih==1){
					}
					else{
						break;
					}
					}while(false);
				}
		}




import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GeometricalFormula {

	    public static void twodimensionalMenu() {
	        System.out.println("Jenis Bangun Ruang Dua Dimensi");
	        System.out.println("1. Persegi");
	        System.out.println("2. Segitiga");
	        System.out.println("3. Persegi Panjang");
	        System.out.println("4. Lingkaran");
	        System.out.println("5. Belah Ketupat");
	        System.out.println("6. Jajar Genjang");
	        System.out.println("7. Trapesium");
	        System.out.println("0. Keluar");
	    }

	    public static void squareformula() {
	        System.out.print("Masukan Panjang Sisi = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside = null;
	        try {
	            inputside = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float side = Float.parseFloat(inputside);
	            double large = Math.pow(side,2);
	            float roving = 4*side;
	            System.out.println("Luas Persegi = " + large);
	            System.out.println("Keliling persegi = "+roving);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Anda Salah Memasukkan Format Data");
	        }
	    }

	    public static void triangleformula() {
	        System.out.print("Masukan Panjang Sisi pertama = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside1 = null;
	        try {
	            inputside1 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        System.out.print("Masukan Panjang Sisi kedua = ");
	        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside2 = null;
	        try {
	            inputside2 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        System.out.print("Masukan Panjang Sisi ketiga = ");
	        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside3 = null;
	        try {
	            inputside3 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float side1 = Float.parseFloat(inputside1);
	            float side2 = Float.parseFloat(inputside2);
	            float side3 = Float.parseFloat(inputside3);
	            float roving = side1+side2+side3;
	            double s = 0.5*roving;
	            double large = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
	            System.out.println("keliling segitiga = " + roving);
	            System.out.println("Luas segitiga = " + large);
	               
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Anda Salah Memasukkan Format Data");
	        }
	    }
	    
	    public static void rectangleformula(){
	    	 System.out.print("Masukan Panjang Sisi = ");
		     BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		     String inputlongside = null;
		     try {
		         inputlongside = bufferedreader.readLine();
		     }
		     catch (IOException error) {
		    	 System.out.println("Error Input " + error.getMessage());
		     }
		     
		     System.out.print("Masukan Lebar Sisi = ");
		     bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		     String inputwide = null;
		     try {
		    	 inputwide = bufferedreader.readLine();
		     }
		     catch (IOException error) {
		    	 System.out.println("Error Input " + error.getMessage());
		     }
		     
		     try {
		    	 float longside = Float.parseFloat(inputlongside);
		    	 float wide = Float.parseFloat(inputwide);
		    	 double large = longside*wide;
		    	 float roving = 2*(longside+wide);
		    	 System.out.println("Luas Persegi Panjang = " + large);
		    	 System.out.println("Keliling Persegi Panjang = "+roving);
		     }
		     catch(NumberFormatException e) {
		    	 System.out.println("Anda Salah Memasukkan Format Data");
		     }
	    }
	    
	    public static void circleformula(){
	    	System.out.print("Masukkan Panjang Jari-jari lingkaran = ");
	    	BufferedReader bufferedreader = new BufferedReader( new InputStreamReader (System.in));
	    	String inputradius = null;
	    	try{
	    		inputradius = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input " + error.getMessage());
	    	}
	    	
	    	try{
	    		float radius = Float.parseFloat(inputradius);
	    		double roving = 2*Math.PI*radius;
	    		double large = Math.PI*Math.pow(radius, 2);
	    		System.out.println("Luas Lingkaran = " + large);
	    		System.out.println("Keliling Lingkaran = "+roving);
	    	}
	    	catch(NumberFormatException e) {
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	    
	    public static void rhombusformula(){
	    	System.out.print("Masukkan Panjang Diagonal Pertama = ");
	    	BufferedReader bufferedreader = new BufferedReader( new InputStreamReader (System.in));
	    	String inputdiagonal1 = null;
	    	try{
	    		inputdiagonal1 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input " + error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang Diagonal Kedua = ");
	    	bufferedreader = new BufferedReader( new InputStreamReader (System.in));
	    	String inputdiagonal2= null;
	    	try{
	    		inputdiagonal2 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input " + error.getMessage());
	    	}
	    	
	    	try{
	    		float diagonal1 = Float.parseFloat(inputdiagonal1);
	    		float diagonal2 = Float.parseFloat(inputdiagonal2);
	    		double large = diagonal1*diagonal2;
	    		double side = Math.sqrt(Math.pow(0.5*diagonal1,2)+Math.pow(0.5*diagonal2, 2));
	    		double roving = 4*side;
	    		System.out.println("Luas Belah Ketupat = " + large);
	    		System.out.println("Keliling Belah Ketupat = "+roving);
	    	}
	    	catch(NumberFormatException e) {
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	    
	    public static void parallelogramformula(){
	    	System.out.print("Masukkan Panjang alas jajar genjang = ");
	    	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputpedestal = null;
	    	try{
	    		inputpedestal = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang Sisi miring jajargenjang = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader (System.in));
	    	String inputhypotenuse = null;
	    	try{
	    		inputhypotenuse = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error " + error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Tinggi Jajar genjang = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader (System.in));
	    	String inputhigh = null;
	    	try{
	    		inputhigh = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error " + error.getMessage());
	    	}
	    	try{
	    		float pedestal = Float.parseFloat(inputpedestal);
	    		float hypotenuse = Float.parseFloat(inputhypotenuse);
	    		float high = Float.parseFloat(inputhigh);
	    		double large = 0.5*high*pedestal;
	    		double roving = 2*(pedestal+hypotenuse);
	    		System.out.println("Luas Jajajr genjang = "+ large);
	    		System.out.println("Keliling Jajar genjang = "+ roving);
	    	}
	    	catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	    
	    public static void trapezoidalformula(){
	    	System.out.print("Masukkan Panjang sisi pertama (sisi sejajar) = alas = ");
	    	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputside1 = null;
	    	try{
	    		inputside1 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang sisi kedua (sisi sejajar) = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputside2 = null;
	    	try{
	    		inputside2 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang sisi ketiga = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputside3 = null;
	    	try{
	    		inputside3 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Panjang sisi keempat = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputside4 = null;
	    	try{
	    		inputside4 = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Tinggi trapesium = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputhigh = null;
	    	try{
	    		inputhigh = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Input Error "+ error.getMessage());
	    	}
	    	
	    	try{
	    		float side1 = Float.parseFloat(inputside1);
	    		float side2 = Float.parseFloat(inputside2);
	    		float side3 = Float.parseFloat(inputside3);
	    		float side4 = Float.parseFloat(inputside4);
	    		float high = Float.parseFloat(inputhigh);
	    		double large = 0.5*(side1+side2)*high;
	    		double roving = side1+side2+side3+side4;
	    		System.out.println("Luas Trapesium = "+large);
	    		System.out.println("Keliling Trapesium = "+roving);
	    	}
	    	catch(NumberFormatException error){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}	
	    }
	    
	    public static void threedimensionalMenu() {
		    System.out.println("Jenis Bangun Ruang Dua Dimensi");
		    System.out.println("1. Balok");
		    System.out.println("2. Kubus");
		    System.out.println("3. Prisma Segitiga");
		    System.out.println("4. Limas Segiempat");
		    System.out.println("5. Tabung");
		    System.out.println("6. Kerucut");
		    System.out.println("7. Bola");
	        System.out.println("0. Keluar");
		    }
	    
	    public static void beamformula(){
	    	System.out.print("Masukkan Panjang balok = ");
	    	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputlong = null;
	    	try{
	    		inputlong = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input "+error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Lebar balok = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputwide = null;
	    	try{
	    		inputwide = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input "+error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Tinggi balok = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputhigh= null;
	    	try{
	    		inputhigh = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input "+error.getMessage());
	    	}
	    	
	    	try{
	    		float longbeam = Float.parseFloat(inputlong);
	    		float wide = Float.parseFloat(inputwide);
	    		float high = Float.parseFloat(inputhigh);
	    		double large = 2*((longbeam*wide)+(longbeam*high)+(wide*high));
	    		double roving = 4*(longbeam+wide+high);
	    		double volume = longbeam*wide*high;
	    		System.out.println("Luas Permukaan balok = "+large);
	    		System.out.println("Keliling Permukaan balok = "+roving);
	    		System.out.println("Volume balok = "+volume);
	    	}
	    	catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	    
	    public static void cubeformula(){
	    	System.out.print("Masukkan Panjang sisi Kubus = ");
	    	BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	    	String inputside = null;
	    	try{
	    		inputside = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.println("Error Input "+ error.getMessage());
	    	}
	    	
	    	try{
	    		float side = Float.parseFloat(inputside);
	    		double large = 6*Math.pow(side, 2);
	    		double roving = 12*side;
	    		double volume = Math.pow(side,3);
	    		System.out.println("Luas Permukaan Kubus = "+large);
	    		System.out.println("Keliling Permukaan Kubus = "+roving);
	    		System.out.println("Volume Kubus = "+volume);
	    	}
	    	catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}	
	    }
	    
	    public static void triangularprismformula(){
	    	System.out.print("Masukan Panjang Sisi alas pertama = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside1 = null;
	        try {
	            inputside1 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        System.out.print("Masukan Panjang Sisi alas kedua = ");
	        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside2 = null;
	        try {
	            inputside2 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        System.out.print("Masukan Panjang Sisi alas ketiga = ");
	        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputside3 = null;
	        try {
	            inputside3 = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        System.out.print("Masukan Tinggi prisma = ");
	        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputhigh = null;
	        try {
	            inputhigh = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float side1 = Float.parseFloat(inputside1);
	            float side2 = Float.parseFloat(inputside2);
	            float side3 = Float.parseFloat(inputside3);
	            float high = Float.parseFloat(inputhigh);
	            float roving = 2*(side1+side2+side3)+3*high;
	            double s = 0.5*roving;
	            double largetriangle = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
	            double large =(side1+side2+side3)*high*2*largetriangle;
	            double volume = largetriangle*high;
	            System.out.println("keliling Prisma Segitiga = " + roving);
	            System.out.println("Luas Prisma Segitiga = " + large);
	            System.out.println("Volume Prisma Segitiga = "+volume);   
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Anda Salah Memasukkan Format Data");
	        }
	    }
	    
	    public static void rectangularpyramidformula(){
	    	System.out.print("Masukkan Panjang sisi alas = ");
	    	BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputsidepedestal = null;
	    	try{
	    		inputsidepedestal = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.print("Input Error "+error.getMessage());
	    	}
	    	
	    	System.out.print("Masukkan Tinggi Limas = ");
	    	bufferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputhigh = null;
	    	try{
	    		inputhigh = bufferedreader.readLine();
	    	}
	    	catch(IOException error){
	    		System.out.print("Input Error "+error.getMessage());
	    	}
	    	
	    	try{
	    		float sidepedestal = Float.parseFloat(inputsidepedestal);
	    		float high = Float.parseFloat(inputhigh);
	    		double largepedestal = Math.pow(sidepedestal,2);
	    		double highside = Math.sqrt(Math.pow(0.5*sidepedestal,2)*Math.pow(high,2));
	    		double largeside = 0.5*sidepedestal*highside;
	    		double large = largepedestal+(4*largeside);
	    		double sidepyramid = Math.sqrt(Math.pow(highside,2)+Math.pow(0.5*sidepedestal,2));
	    		double roving = (4*sidepedestal)+(4*sidepyramid);
	    		double volume = (1/3)*largepedestal*high;
	    		System.out.println("Luas Limas Segiempat = "+ large);
	    		System.out.println("Keliling Limas Segiempat = "+ roving);
	    		System.out.println("Volume Limas Segiempat = "+ volume);
	    	}
	    	catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	    
	    public static void tubeformula(){
	    	System.out.print("Masukan Jari-jari = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputradius = null;
	        try {
	            inputradius = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        System.out.print("Masukan Tinggi tabung = ");
	        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputhigh = null;
	        try {
	            inputhigh = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        try{
	        	float radius = Float.parseFloat(inputradius);
	        	float high = Float.parseFloat(inputhigh);
	        	double large = 2*Math.PI*radius*(radius+high);
	        	double volume = Math.PI*Math.pow(radius,2)*high;
	        	System.out.println("Luas Tabung = "+large);
	        	System.out.println("Volume Tabung = "+volume);
	        }
	        catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	 
	    public static void coneformula(){
	    	System.out.print("Masukan Jari-jari = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputradius = null;
	        try {
	            inputradius = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        System.out.print("Masukan Tinggi Kerucut = ");
	        bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputhigh = null;
	        try {
	            inputhigh = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        try{
	        	float radius = Float.parseFloat(inputradius);
	        	float high = Float.parseFloat(inputhigh);
	        	double side = Math.sqrt(Math.pow(high, 2)+Math.pow(radius, 2));
	        	double large = Math.PI*radius*(radius+side);
	        	double volume = 1/3*Math.PI*Math.pow(radius,2)*high;
	        	System.out.println("Luas Tabung = "+large);
	        	System.out.println("Volume Tabung = "+volume);
	        }
	        catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	    
	    public static void ballformula(){
	    	System.out.print("Masukan Jari-jari = ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputradius = null;
	        try {
	            inputradius = bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        try{
	        	float radius = Float.parseFloat(inputradius);
	        	double large = 4*Math.PI*Math.pow(radius, 2);
	        	double volume = (3/4)*Math.PI*Math.pow(radius,3);
	        	System.out.println("Luas Bola = "+large);
	        	System.out.println("Volume Bola = "+volume);
	        }
	        catch(NumberFormatException e){
	    		System.out.println("Anda Salah Memasukkan Format Data");
	    	}
	    }
	    
		public static void main(String[] args) {
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputData = null;
	        int choice = 0;
	        int choice1 = 0;
	        int choice2 = 0;
	        do {
	        	System.out.println("Rumus Bangun Ruang Dua dan Tiga Dimensi");
	        	System.out.println("1. Bangun Ruang Dua Dimensi");
	        	System.out.println("2. Bangun Ruang Tiga Dimensi");
	        	System.out.print("\nMasukkan Pilihan Anda = ");
	        	try {
	                inputData = bufferedReader.readLine();
	                try  {
	                    choice = Integer.parseInt(inputData);
	                    if (choice > 0 && choice == 1) {
	                    	twodimensionalMenu();
	                    	try {
	                    		System.out.print("Masukkan Pilihan Anda = ");
	        	                inputData = bufferedReader.readLine();
	        	                try  {
	        	                    choice1 = Integer.parseInt(inputData);
	        	                    if (choice1 > 0 && choice1 == 1) {
	        	                        squareformula();
	        	                    }
	        	                    else if (choice1 > 0 && choice1 == 2) {
	        	                    	triangleformula();
	        	                    }
	        	                    else if (choice1 > 0 && choice1 == 3) {
	        	                        rectangleformula();
	        	                    }
	        	                    else if (choice1 > 0 && choice1 == 4) {
	        	                        circleformula();
	        	                    }
	        	                    else if (choice1 > 0 && choice1 == 5) {
	        	                        rhombusformula();
	        	                    }
	        	                    else if (choice1 > 0 && choice1 == 6) {
	        	                    	parallelogramformula();
	        	                    }
	        	                    else if (choice1 > 0 && choice1 == 7) {
	        	                        trapezoidalformula();
	        	                    }
	        	                    else if(choice1 == 0){
	        	                    	break;
	        	                    }
	        	                }
	        	                catch(NumberFormatException e) {
	        	                    System.out.println("Masukan Anda Tidak Sesuai\n");
	        	                }
	        	                
	        	            }
	        	            catch (IOException error) {
	        	                System.out.println("Error Input " + error.getMessage());
	        	            }
	                    }
	                    else if (choice > 0 && choice == 2){
	                    	threedimensionalMenu();
	                    	try {
	                    		System.out.print("Masukkan Pilihan Anda = ");
	        	                inputData = bufferedReader.readLine();
	        	                try  {
	        	                    choice2 = Integer.parseInt(inputData);
	        	                    if (choice2 > 0 && choice2 == 1) {
	        	                        beamformula();
	        	                    }
	        	                    else if (choice2 > 0 && choice2 == 2) {
	        	                    	cubeformula();
	        	                    }
	        	                    else if (choice2 > 0 && choice2 == 3) {
	        	                    	triangularprismformula();
	        	                    }
	        	                    else if (choice2 > 0 && choice2 == 4) {
	        	                    	rectangularpyramidformula();
	        	                    }
	        	                    else if (choice2 > 0 && choice2 == 5) {
	        	                        tubeformula();
	        	                    }
	        	                    else if (choice2 > 0 && choice2 == 6) {
	        	                    	coneformula();
	        	                    }
	        	                    else if (choice2 > 0 && choice2 == 7) {
	        	                        ballformula();
	        	                    }
	        	                    else if(choice2 == 0){
	        	                    	break;
	        	                    }
	        	                }
	        	                catch(NumberFormatException e) {
	        	                    System.out.println("Masukan Anda Tidak Sesuai\n");
	        	                }
	        	                
	        	            }
	        	            catch (IOException error) {
	        	                System.out.println("Error Input " + error.getMessage());
	        	            }
	                    }
	                }
	                catch(NumberFormatException e) {
	                    System.out.println("Masukan Anda Tidak Sesuai");
	                }
	            }
	            catch (IOException error) {
	                System.out.println("Error Input " + error.getMessage());
	            }
	        } while(choice > 0);
	    }
	}

